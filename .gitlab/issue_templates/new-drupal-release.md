This issue provides a checklist for updating Drutopia Host when a new release of Drupal is released.  (A new release of the Drutopia distribution should not require updates here).

## Work Required

* [ ] Save this issue first
* [ ] [Consult the documentation](http://docs.drutopia.org/en/latest/updating-drutopia-host.html) for each step  TODO write the documentation
* [ ] Add Drupal point version to this issue's title 
* [ ] Identify if any scaffolding changed from the previous version

### Scaffolding changes that need template changes

#### web/default/settings.default.php

If the section that looks something like this (commented out) 

```
  if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
     include $app_root . '/' . $site_path . '/settings.local.php';
  }
```

changes, it will need to be updated in enable settings.local.php include_tasks in https://gitlab.com/drutopia/drutopia_host/blob/master/roles/drutopia/tasks/build.yml


### Build and test

* [ ] Deployment of a test site works

### Tag and release

* [ ] Create new release of Drutopia Host

/title Update Ansible templates to match Drupal 8. scaffolding files
/label ~Drupal-in-Ansible
