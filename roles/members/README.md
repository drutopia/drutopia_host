# Members Role

## Introduction

This role is intended to manage the individual installations of a Drutopia release.

When run in managed mode (`user_managed_site = False` [the default]), a managed release is specified and the locally-sourced release is "activated" with a specific `activate.sh` script as built by the `druptopia` role. See that role for further information on the build process.

Alternatively, this role can set up a user's folder structure (and related system services) to configure a `user_managed_site`. In this case, the `~/site` folder is user-writable. This option allows deployment of whatever the user would like to the ~/site/web folder, and this is often used with our `amanita` tools to deploy via gitlab-ci.

## Primary role operations

For each user, the following can be configured by this role:

- Apache2 site configuration for the user with numerous options
- Required MariaDB/MySQL database(s)
- Active PHP version, both for CLI and FPM
- A sync target (i.e. test site) that `sync_to_test.sh` will be able to sync to.
- Certbot certificate configuration
- Drush configuration
- cron jobs and more...

The base system software for PHP, Apache, and MariaDB are installed by the corresponding php, apache, and mysql roles.

The approximate control flow is as follows:
- Loop over the sites in members (potentially limiting to `target_member`)
- Set up role variables per the member variables and defaults
- Set up the individual member:
  - Create the user/database(s)/apache/fpm/etc items
  - Perform backups
  - Deploy updated code and configuration
  - Perform drupal updates/config import

Again, if `user_managed_site` is true, only the first step is run (creating the user/db/etc) and the site folder left user-writable.

## Control variables

In addition to the specific settings for each member, a few additional variables can be used per-execution of `ansible-playbook` (using `-e`/`--extra-vars`) in order to deal with special circumstances on a one-off basis:

- An individual member site can be targeted by setting `target_member` to the desired member name
- When the configuration of the site does not match the current config directory, the deployment can be forced to continue by supplying `config_import_force_all`.
- If the existing site is broken/cannot run drush, you can skip running backups before the code is upgraded by setting `skip_all_backups` to true.

These variables can be set directly to Python truthy values - they will be cast to boolean by the role (so you do not have to use json format for the extra-vars parameter if you don't want to).

For example: `ansible-playbook -i inventory_live provision.yml -e "target_member=example_live config_import_force_all=true skip_all_backups=true" --skip-tags=setup,build,archive,push` will deploy the example_live site, whether it is currently functional (and has matching configuration) or not.

## Settings Overview

The way the primary Drutopia playbook is configured, the settings for each hosting configuration are read from the sites list of dictionaries. Each site is structured as follows, with the key being used for primary elements of each site (i.e. it is used as the `drutopian`):

```yaml
members:
  site_name:
    drutopia_version: stable
    drupal_install: true
    update_method: deploy
    etc...
  another_site:
    drupal_install: false
    online: true
```

To use this role independent of Drutopia, all options should be specified directly. The only other requirement (aside from the base software) is the meta requriement for the controls role.

## Folder Structure

The following folder structure is created by this role:

```
<member_root(/home)/<user_name>/:
├── .drush/
│  └── drush.yml
├── .drutopia/
│   ├── <user_name>-fpm-pool.conf # linked from fpm pool.d when site online
│   └── <user_name>-vhost.conf #linked from apache sites-enabled when site online
├── .ssh/
│   ├── config # set up to point to test-site, when sync_target is configured
│   └── authorized_keys # contains all ssh_authorized_keys
├── backups/ # system-generated sql dumps
├── bin/
│   ├── php     # linked to selected php version
│   ├── drush   # linked to ../site/vendor/bin/drush
│   └── sync_to_test.sh # optional if a sync_target is configured
├── custom/ - git checkout of user source
│   ├── themes/
│   ├── custom/
│   │   └── some_theme # overlaid into site/web/custom/themes/custom/some_theme
│   └── config/ - copied onto site/config
│       └── (configuration data)
├── files/ (public files folder)
└── site/
    ├── web/
    │   ├── sites/default/
    │   │   ├── settings.local.php  # drutopia-managed site configuration
    │   │   └── files -> symlink to ~/files
    │   └── vendor/
    ├── .htpasswd       # optional if htpasswd protection is enabled
    ├── composer.json
    ├── composer.lock
    ├── redirects.map   # optional if a rewrite map is configured
    ├── VERSION         # contains tag of drutopia build currently deployed
    └── CUSTOM_VERSION  # contains tag of custom_repo currently deployed
```

Additionally, a database user and database of the same name as the user (by default) are created.

Defaults for many settings are documented as comments in the `defaults/main.yml`. Additional per-site defaults can also be seen in the `dereference.yml` which is used to set role variables according to a members map item. These defaults are NOT set when using the role outside of Drutopia.

## Custom Files

While individual member sites are based on a shared release/codebase, they are individually configured via the user's custom folder. This folder may be populated one of two ways: a per-member repository, or a local folder (`master_custom_path`) with per-member config folders (or `custom_path`).

The `config` folder, and the `theme/custom/*` folders in the custom folder are overlaid on the release.

## Base Member Settings

- `delete_member`: When true, any existing member by this name will be deleted, including databases
- `member_temp`: Optionally set to some directory name, for example `tmp` in order to configure a per-user temp folder used by PHP and other things.
- `online`: Determines whether or not the site's fpm and apache configurations will be active
- `ssh_authorized_keys`: A list of ssh public keys allowed to login to the host via SSH as this user e.g. `['https://gitlab.com/{your user}.keys', 'ssh RSA:AANsjtnw...etc']`
- `user_managed_site`: When true, the user's site folder will be writable and no web site will be installed/configured
- `web_subfolder`: The name of the drupal web folder (common alternative might be: public_html), default: `web`

## SSL settings

It is recommended to set up all DNS ahead of time, and ensure the site is operational under http before configuring SSL.

certbot_enable - when true, allow certbot to acquire a certificate for all host names for this site.

If certbot is not used, a server-wide certificate can be used (`ssl_cert_path`, `ssl_chain_path`, `ssl_key_path`) as a default, for example when all hosts are subdomains: `<some name>.example.com`. Lastly, a per-site certificate can be set using:
- `site_ssl_cert_path`
- `site_ssl_chain_path`
- `site_ssl_key_path`

Additionally, HSTS can also be enabled. Legacy SSL protocols should already be disabled (via `/etc/apache2/conf-enabled/ssp-protocols.conf`) and headers should be enabled. Individual sites can opt into setting the HSTS header by setting `site_ssl_hsts: true`

## HTTP settings

- `canonical_redirect`: when true, all site requests to `server_aliases` names will redirect to `server_hostname`.
- `custom_redirect`: true: add `redirects.map` to http config (compiled from `redirects.txt` by activation script)
- `custom_vhost`: include a .drutopia/<drutopian>-vhost-custom.conf in the http config
- `file_fallback_member`: when set, the name of another member on the host that should be used to locate missing files from the public files folder (in order to avoid having to sync all files from a live site to a test site, i.e. typically this will be `<this member>_live`)
- `server_aliases`: a list of additional host names for the site
- `server_hostname`: the canonical host name to listen for this site

### Basic Password Protection

You can prevent a site (usually test/pre-release) from being accessible publicly by configuring the htpasswd variables. This will cause the role to generate a ~/site/.htpasswd file with the supplied user/password. This configures only a single user. If the user name is changed, it will NOT remove the previous user, instead creating an additional user/password entry in the `.htpasswd` file. All three variables: enable/name/password, must be set (+true for enable) for the password prompt to be enabled.

- `htpasswd_enable`: when true, create an .htpasswd file and configure authentication
- `htpasswd_name`: the user name
- `htpasswd_password`: the required password
- `htpasswd_prompt`: prompt used by the password request

### `.htaccess` Customization

The build deployed should contain the desired .htaccess file, however this can be customized using the following settings.

- `htaccess_mod`: custom modifications for the `~/site/web/.htaccess` file

Only one of these should be set:
- `htaccess_mod_after`: a string in .htaccess to insert htaccess_mod after
- `htaccess_mod_before`: a string in .htaccess to insert htaccess_mod before

## Customization Sources

These are the sources that are put into the user's `/custom` folder.
- `custom_path`: set this to use a local path for per-site configuration (along with `master_custom_path`)
- `custom_repo`: set this to a repo (e.g. git@gitlab.com/myorg/mysite.git)
- `custom_version`: the branch|tag|commit to deploy
- `git_ssh_key`: the private key to use in order to read the custom_repo sources. Will default to a global/host level `git_ssh_key`.

Using a local custom_path might look like:

```
drutopia_host/
└──config_master/
   └──site-a/
      └──config
         └──(config yaml files)
```

## Database Settings

### Basic Database Settings

- `db_disable`: set to disable database creation
- `db_name`: the database name to create, by default is the same as the member name
- `db_pwd`: a strong password for the main database
- `db_user`: the user with access to db_name and db_migrate_name, by default is the same as the member name
- `db_migrate_name`: an additional database to create (intended to hold migration sources)

### Backups

- `db_backup_disable`: set to disable scheduled backups
- `db_backup_days`: a cron schedule for the days backups are run
- `db_backup_hours`: a cron schedule for the hour backups are run
- `db_backup_minutes`: a cron schedule for the hour backups are run

See [https://crontab.guru](https://crontab.guru) for an easy configuration tester.

## Drupal Settings

### Additional Settings for PHP/settings.local.php

- `db_migrate_key`: the key used in the databases array to refer to the `db_migrate_name` database.
- `hash_salt`: the site's hash salt
- `user_register`: provide setting for $config['user.settings']['register']
- `fpm_slowlog`: set to enable the fpm slow log
- `php_settings_code`: as a last resort, use this to add custom code to an individual settings file. Other alternatives for this are setting up a custom jinja template for the settings.local.php or including code in the source codebase in settings.php.

#### Extra vars

`extra_vars` - An additional variable that may be used to specify customized settings for each site. These can then be referenced in your own particular `settings.local.php.j2` template.

For example, you might have:
```yaml
  test_site:
    drutopia_version: stable
    drupal_install: true
    update_method: deploy
    extra_vars:
      module_a_tricks: "TRUE"
    ...etc
```

Then set your `settings_local_template` variable (in e.g. your host_vars) to whatever settings source template you'd like to use, and in that template, reference your additional settings:

```php
{% if extra_vars.module_a_tricks is defined and extra_vars.module_a_tricks|length %}
$config['module_a.settings']['cool_tricks'] = {{ extra_vars.module_a_tricks }};
{% endif %}
```

### Drupal Installation

This role will attempt to detect that an existing Drupal site is present. This is done by attempting to run a `drush status --fields=bootstrap` for the existing site. If this command can run and returns `Drupal bootstrap` in it's `stdout`, the site is considered to be installed.

- `drupal_install`: when true (and the site is not presently detected as installed), perform an install.
- `drupal_force_reinstall`: perform the install regardless if an active installation is detected.

The following are the install command options:
- `drupal_account_name`
- `drupal_account_pass`
- `drupal_email`
- `drupal_profile`
- `drupal_site_name`

Additional installation features:
- `drupal_enable_modules`: after install, the listed modules will be enabled via drush (generally better to use configuration), e.g. `[migrate, migrate-plus]`
- `drupal_install_args`: additional arguments to supply to the `drush si` command.
- `drupal_migrate`: command to run after install to perform a migration, e.g. `mim` (do not specify drush itself, just the command)
- `drush_post_install_cmds`: a list of commands to run (via drush). e.g. `['cr', 'mim']` (do not specify drush itself, just the commands)

### Drupal Config

- `config_import`: NO LONGER SUPPORTED: select an update_method instead.
- `config_import_force`: when true, even if the system configuration has changed since what was imported in the previous deployment, config import will run (otherwise the deploy will stop)
- `config_sync_path`:efaulting to `../config/`, the path to the config yaml files.
- `config_sync_var`: used for legacy sites, when set to 'config_directories' the settings.local.php config var will be set in Drupal 7/8 style. When set to 'settings' (default), the `$settings['config_sync_directory']` will be set
- `site_uuid`: used to force a specific site UUID. It is better to use `--existing-config`, but this can be used after an install has been performed/some database exists.

## Build Selection

Individualized site code is not installed for each site. Instead, a build already created and pushed to the server with the `drutopia` role should be referenced. It is perfectly valid to have a per-site build specified in that `drutopia_versions` listing that is used by a single/pair of site(s). In other words: both the build and the site would have the same git source values specified for their git_repo and custom_repo settings respectively.

- `drutopia_version`: the drutopia build to install. This key should be set to the desired value from the list of values used for the `drutopia_versions` setting.

## Drupal Deployment

After activation of the new code on a member website, the drupal commands to update it must be run. This phase is controlled by the `update_method` and related variables.

- `update_method`: May be one of: `none`, `script`, `dbupdate`, `deploy`, or `custom`:

| update_method | action |
| ------------- | ------ |
| none | nothing is done! |
| dbupdate | drush cr; drush updb -y |
| deploy | drush deploy |
| script | the supplied script is executed at the site_root (i.e. `~/site/`)
| custom | assign drupal_deploy to a list of drush actions, e.g. `['cr','updb -y','cim -y']` |

## Cron

Set a cron schedule for `drush cron` (the day is awlays `*`)

- `drush_cron_disable`: when true, disable/don't create scheduled drush cron
- `drush_cron_hour`: cron schedule, e.g. `"*"` for all hours
- `drush_cron_minute`: cron schedule, e.g. `"*/10"` for every 10 minutes

## Deploy 'hooks'

Run additional shell commands (as the site user) at various points during an install.

- `pre_deploy`: commands run after backup, but before code updates.
- `post_deploy`: commands run after code updates, before Drupal (e.g. `drush updb`) commands
- `post_drupal`: commands run after code and db are updated.

Each setting may contain a list of maps. Each entry is either a drush command to run or a shell command (depending on action)

```yaml
    # ....
    post_drupal:
      - command: "whoami && pwd"
        chdir: "~/bin"
        changed_when: false
        ignore_errors: true
        action: shell
      - command: "cr"
        action: drush
```

## Synchronization Settings

One may have multiple instances of a particular site, for example, an example_live and example_test. In this case, the example_live can be set up to synchronize its current database and files to the example_test instance, useful for deployment testing. The `sync_to_test.sh` script uses a target named `test-site` that is configured in the `.ssh/config` file for the ssh/rsync commands.

- `sync_files`: when true, the files will also sync to the target instance
- `sync_hostname`: defaults to localhost for ssh/rsync to the other member site
- `sync_member`: when set to the name of another member, the system will create the `sync_to_test.sh` and related configuration (e.g. ssh keys)
- `sync_rm_files`: when set, the target's files folder is cleared. This is used in concert with the test site's file_fallback_member (which would be e.g. example_live) to avoid syncing files between sites.
- `sync_styles`: when set, the `files/styles` folder will also rsync. This was added to support Gutenberg not generating additional image file styles.

Limitations:
- the role does not verify that the target `sync_member` is within the members key.
- the role can configure the authorized_keys only when `sync_hostname == ansible_fqdn` (default per `dereferece.yml`)
