---
- name: cache rebuild, because it seems always needed for drush to behave
  command: "{{ drush_path }} --root={{ drupal_dir }} cr"
  args:
    chdir: "{{ drupal_dir }}"
  changed_when: false
  ignore_errors: true

- name: (re)check site is installed using new codebase
  command: "{{ drush_path }} --root={{ drupal_dir }} status --format=string --fields=bootstrap"
  args:
    chdir: "{{ drupal_dir }}"
  register: drupal_site_installed
  failed_when: drupal_site_installed.stdout is undefined or drupal_site_installed.rc != 0
  changed_when: false

- name: set flag drupal_installed
  set_fact:
    drupal_installed: "{{ 'Successful' in drupal_site_installed.stdout }}"

- name: clear flag drupal_installed, forcibly re-installing
  set_fact:
    drupal_installed: false
  when: drupal_force_reinstall

- name: run install, as needed/requested
  block:
    - name: install drupal with drush
      command: >
        {{ drush_path }} site-install -y
        --site-name="{{ drupal_site_name }}"
        --account-name="{{ drupal_account_name }}"
        --account-pass="{{ drupal_account_pass }}"
        --account-mail={{ drupal_email }}
        --root={{ drupal_dir }}
        {{ drupal_profile }}
        {{ drupal_install_args }}
      args:
        chdir: "{{ drupal_dir }}"

    - name: Install configured modules with drush.
      command: >
        {{ drush_path }} pm-enable -y {{ drupal_enable_modules | join(" ") }}
        --root={{ drupal_dir }}
      args:
        chdir: "{{ drupal_dir }}"
      when: drupal_enable_modules|default([])|length>0

    - name: Run any extra post-install drush commands
      command: >
        {{ drush_path }}
        {{ drush_cmd }}
        --root={{ drupal_dir }}
      args:
        chdir: "{{ drupal_dir }}"
      when: drush_post_install_cmds|length>0
      loop: "{{ drush_post_install_cmds }}"
      loop_control:
        loop_var: drush_cmd

    - name: Run any migration commands
      command: >
        {{ drush_path }} {{ drupal_migrate }}
        --root={{ drupal_dir }}
      args:
        chdir: "{{ drupal_dir }}"
      when: drupal_migrate is defined and drupal_migrate|length > 0

    - name: set flag drupal_installed
      set_fact:
        drupal_installed: true
  when: "drupal_install and not drupal_installed"
  become: true
  become_user: "{{ drutopian }}"
  rescue:
    - name: skip remaining tasks for member
      set_fact:
        skip_member: true
  tags: drupal,members

- name: set drupal tasks for update_method none or script
  set_fact:
    drupal_deploy: []
  when: update_method in ("none", "script")
  tags: drupal,members

- name: set drupal tasks for update_method dbupdate
  set_fact:
    drupal_deploy: ["cr", "updb -y"]
  when: update_method == "dbupdate"
  tags: drupal,members

- name: set drupal tasks for update_method deploy
  set_fact:
    drupal_deploy: ["deploy -y"]
  when: update_method == "deploy"
  tags: drupal,members

- name: fail when update_method custom and no methods set
  fail:
  when: update_method == "custom" and not drupal_deploy
  tags: drupal,members

- name: run update tasks when site installed
  block:
    - name: call drush commands
      shell: "{{ drush_path }} {{ drush_cmd }}"
      args:
        chdir: "{{ drupal_dir }}"
      loop: "{{ drupal_deploy }}"
      loop_control:
        loop_var: drush_cmd

    - name: run custom update script
      shell: "{{ update_script }}"
      args:
        chdir: "{{ site_dir }}"
      when: update_method == "script" and update_script

    - name: config set site uuid
      shell: "{{ drush_path }} cset system.site uuid {{ site_uuid }} -y"
      args:
        chdir: "{{ drupal_dir }}"
      when: site_uuid|length > 0

    - name: check for translations
      find:
        path: "{{ site_dir }}/translations"
        patterns:
          - "custom-translations.*.po"
      register: translation_files

    - name: install translations when found
      block:
        - name: split out translation name
          set_fact:
            translations[file.path]: "{{ file.path | regex_replace('^custom-translation\\.(.*)\\.po$', '\\1')  }}"
          loop: "{{ translation_files.files }}"
          loop_control:
            loop_var: file

        - name: import translations
          shell: "{{ drush_path }}  locale-import {{ translations[file.path] }} {{ file.path }} --type=customized --override=all"
          loop: "{{ translation_files.files }}"
          loop_control:
            loop_var: file
      when: "not translation_files.failed and translation_files.matched > 0"

    - name: follow-up cache rebuild
      shell: "{{ drush_path }} cr"
      args:
        chdir: "{{ drupal_dir }}"
  when: "drupal_installed and not skip_member"
  become: true
  become_user: "{{ drutopian }}"
  rescue:
    - name: skip remaining tasks for member
      set_fact:
        skip_member: true
  tags: drupal,members
